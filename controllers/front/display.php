<?php

class MycookiebannerDisplayModuleFrontController extends ModuleFrontController //bylo mymoduledisplayModuleFrontController
//Mymodule to nazwa modulu, Display to nazwa pliku
{
	public function initContent() //the purpose of initContent() - Its purpose should be assigning the variables to smarty, and setting the template to be displayed.
	{
    	    parent::initContent();
    	    $this->setTemplate('module:mycookiebanner/views/templates/front/display.tpl');
					// $this->module  - is the instance of a module responsible of this controller
					//$this->context  - delivers the result of Context::getContext()
	}
}
