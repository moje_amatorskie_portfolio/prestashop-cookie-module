<!-- Block mymodule -->
<div id="mycookiebanner_block_home" class="block cookie-lawe" >
  <h4 hidden>{l s='Welcome!' mod='mycookiebanner'}</h4>
  <div class="block_content">
    <ul>
      <li hidden><a href="{$my_cookie_banner_link}" title="Click this link">ta polityczka ciastek</a></li>
    </ul>
    {if isset($message) && $message}
    <h1>
      <div class="alert-success " style="text-align:center;font-size:44px;width:100%;">
        {$message}
      </div>
    </h1>
    {/if}
  </div>
</div>
<!-- /Block mymodule -->
